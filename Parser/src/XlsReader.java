import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

/**
 * Created by brice on 21/05/17.
 */

public class XlsReader
{
    private XSSFWorkbook workbook;
    private FileWriter out;

    private final int START_INDEX = 2;
    private final int SHEETS_NUMBER = 12;
    private final String SEPARATOR = " | ";

    public XlsReader(String fileIn, String fileOut)
    {
        try
        {
            InputStream in = new FileInputStream(fileIn);
            this.workbook = (XSSFWorkbook) WorkbookFactory.create(in);
            this.out = new FileWriter(fileOut);
            System.out.println("Opening file");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void readFile()
    {
        ArrayList<Entry> entries = new ArrayList<>();
        for (int i = START_INDEX; i < START_INDEX + SHEETS_NUMBER; i++)
        {
            Sheet sheet = this.workbook.getSheetAt(i);
            if (sheet != null)
            {
                System.out.println("Parsing sheet " + sheet.getSheetName() + "...");
                Row row;
                Iterator<Row> it = sheet.rowIterator();
                if (it.hasNext())
                {
                    it.next();
                }
                int j = 2;
                while (it.hasNext())
                {
                    row = it.next();
                    if (row.getCell(0) != null)
                    {
                        System.out.println("--- Parsing row " + j + "...");

                        int number = (int) row.getCell(0).getNumericCellValue();
                        int trackId = (int) row.getCell(2).getNumericCellValue();
                        int plenary = (i == START_INDEX ? 1 : 0);
                        int sessionId = (int) row.getCell(4).getNumericCellValue();
                        String chair = row.getCell(11).getStringCellValue();
                        int duration = (int) row.getCell(26).getNumericCellValue();
                        Cell c = row.getCell(32);
                        String utc = (c.getCellTypeEnum() == CellType.STRING ? c.getStringCellValue() : String.valueOf(c.getNumericCellValue()));
                        String constraintDay1 = row.getCell(33).getStringCellValue();
                        String constraintDay2 = row.getCell(34).getStringCellValue();
                        c = row.getCell(35);
                        String constraintDay3 = (c != null ? c.getStringCellValue() : "");

                        if (number > 0)
                        {
                            entries.add(new Entry(number, trackId, plenary, sessionId, chair, duration, utc, parseConstraint(constraintDay1), parseConstraint(constraintDay2), parseConstraint(constraintDay3)));
                        }
                        j++;
                    }
                }
            }
        }
        entries.sort(new Comparator<Entry>()
        {
            @Override
            public int compare(Entry e1, Entry e2)
            {
                int trackDiff = Integer.compare(e1.getTrackId(), e2.getTrackId());
                if (trackDiff !=0)
                {
                    return trackDiff;
                }
                else
                {
                    return Integer.compare(e1.getSessionId(), e2.getSessionId());
                }
            }
        });
        for (Entry e : entries)
        {
            try
            {
                this.out.write(e.getPaperId() + SEPARATOR);
                this.out.write(e.getTrackId() + SEPARATOR);
                this.out.write(e.getPlenary() + SEPARATOR);
                this.out.write(e.getSessionId() + SEPARATOR);
                this.out.write(e.getChair() + SEPARATOR);
                this.out.write(e.getDuration() + SEPARATOR);
                this.out.write(e.getUtc() + SEPARATOR);
                this.out.write(e.getConstraintDay1() + SEPARATOR);
                this.out.write(e.getConstraintDay2() + SEPARATOR);
                this.out.write(e.getConstraintDay3() + SEPARATOR);
                this.out.write("\n");
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }
        try
        {
            this.out.flush();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private String parseConstraint(String constraint)
    {
        StringBuilder res = new StringBuilder();
        constraint = constraint.trim();

        if (!constraint.isEmpty())
        {
            String[] split = constraint.replace("[", "").replace("]", "").split(",");
            for (int i = 0; i < split.length; i += 2)
            {
                try
                {
                    double start = Double.parseDouble(split[i])%24;
                    double end = Double.parseDouble(split[i+1])%24;
                    int startInteger = (int) start;
                    double startDecimal = start - startInteger;
                    int endInteger = (int) end;
                    double endDecimal = end - endInteger;

                    DecimalFormat format = new DecimalFormat("00");

                    res.append(format.format(startInteger));
                    res.append(":");
                    res.append(format.format(startDecimal*60));
                    res.append("-");
                    res.append(format.format(endInteger));
                    res.append(":");
                    res.append(format.format(endDecimal*60));
                }
                catch (NumberFormatException e)
                {
                    res.append(split[i]);
                    res.append('-');
                    res.append(split[i+1]);
                }
                finally
                {
                    res.append(',');
                }
            }
        }
        return res.toString();
    }
}