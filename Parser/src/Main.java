import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Created by brice on 21/05/17.
 */
public class Main
{
    public static void main(String[] args)
    {
        XlsReader reader = new XlsReader("resources/tracks.xlsx", "resources/papers.txt");
        reader.readFile();
    }
}
