/**
 * Created by brice on 24/05/17.
 */
public class Entry
{
    private int paperId;
    private int trackId;
    private int plenary;
    private int sessionId;
    private String chair;
    private int duration;
    private String utc;
    private String constraintDay1;
    private String constraintDay2;
    private String constraintDay3;

    public int getPaperId()
    {
        return paperId;
    }

    public void setPaperId(int paperId)
    {
        this.paperId = paperId;
    }

    public int getTrackId()
    {
        return trackId;
    }

    public void setTrackId(int trackId)
    {
        this.trackId = trackId;
    }

    public int getPlenary()
    {
        return plenary;
    }

    public void setPlenary(int plenary)
    {
        this.plenary = plenary;
    }

    public int getSessionId()
    {
        return sessionId;
    }

    public void setSessionId(int sessionId)
    {
        this.sessionId = sessionId;
    }

    public String getChair()
    {
        return chair;
    }

    public void setChair(String chair)
    {
        this.chair = chair;
    }

    public int getDuration()
    {
        return duration;
    }

    public void setDuration(int duration)
    {
        this.duration = duration;
    }

    public String getUtc()
    {
        return utc;
    }

    public void setUtc(String utc)
    {
        this.utc = utc;
    }

    public String getConstraintDay1()
    {
        return constraintDay1;
    }

    public void setConstraintDay1(String constraintDay1)
    {
        this.constraintDay1 = constraintDay1;
    }

    public String getConstraintDay2()
    {
        return constraintDay2;
    }

    public void setConstraintDay2(String constraintDay2)
    {
        this.constraintDay2 = constraintDay2;
    }

    public String getConstraintDay3()
    {
        return constraintDay3;
    }

    public void setConstraintDay3(String constraintDay3)
    {
        this.constraintDay3 = constraintDay3;
    }

    public Entry(int paperId, int trackId, int plenary, int sessionId, String chair, int duration, String utc, String constraintDay1, String constraintDay2, String constraintDay3)
    {
        this.paperId = paperId;
        this.trackId = trackId;
        this.plenary = plenary;
        this.sessionId = sessionId;
        this.chair = chair;
        this.duration = duration;
        this.utc = utc;
        this.constraintDay1 = constraintDay1;
        this.constraintDay2 = constraintDay2;
        this.constraintDay3 = constraintDay3;
    }
}
