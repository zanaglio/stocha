#include <string>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <ctime>


using namespace std;

// Daylight saving time (1 if you are working with start and end dates in summer time, 0 with dates in winter time)
#define DST 1

// Start date of the conference
#define START "09/30/2015 06:00"

// End date of the conference (MUST BE GREATHER THAN START DATE)
#define END "10/01/2015 23:30"

// Format for both start and end dates of the conference : month/day/year hour:minutes
#define DATE_FORMAT "%m/%d/%Y %H:%M"

//stream paper
ifstream fPapers;


//struct Constraint (to work in an easier way with constraints)
typedef struct{

	tm start;
	tm end;

}Constraint;



//struct Paper (multiple Papers in one session)
typedef struct{
	
	int paperId;
	int sessionId;
	int trackId;
	int durationTime;
	int plenary;
	char* chairName;
	double timeZone;
	int passageOrder;
	vector<Constraint> constraintsDay1;
	vector<Constraint> constraintsDay2;
	vector<Constraint> constraintsDay3;

}Paper;

//struct Session (multiple sessions in one track)
typedef struct{
	
	int sessionId;
	int isPlenary;
	vector<Paper> papers;
	tm start;
	tm end;

}Session;


//struct Track
typedef struct{

	int trackId;
	vector<Session> sessions;
	
}Track;


tm startDate;

tm endDate;

vector<tm> dates;


//Global var
vector<Paper> papers;
vector<Session> sessions;
vector<Track> tracks;

int minutes[12] = {0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55};


vector<string> split(string s, string delimiter)
{
	vector<string> res;
	size_t pos = 0;
	string token;

	while ((pos = s.find(delimiter)) != std::string::npos) 
	{
	    token = s.substr(0, pos);
	    res.push_back(token);
	    s.erase(0, pos + delimiter.length());
	}
	res.push_back(s);

    return res;
}

bool contains(vector<int> l, int n)
{
	int i;
	bool res=false;
	for(i=0;i<l.size();i++)
	{
		if(l.at(i)==n)
		{
			res=true;
			break;
		}
			
	}
	return res;
}



//Read track File
//Paper ID | E-Track ID | Plenary | E-Session ID | Chair E-Session | Duration | UTC time | Times Constraints - Day 1 | Times Constraints - Day 2 | Times Constraints - Day 3
void readPapersFile(){
	
	fPapers.open("../Parser/resources/papers.txt", ios::in);
	string line;
	int k=0;
	
	if (fPapers.is_open()) 
	{
		while (getline(fPapers,line)) 
		{
			Paper p;



			//delete the last '|'
			line.erase(line.length()-2,line.length()-1);

			char * cstr = new char [line.length()+1];

			strcpy (cstr, line.c_str());

			char * s;
			s = strtok (cstr,"|"); p.paperId = atoi(s);   
			s = strtok (NULL,"|"); p.trackId = atoi(s);  
			s = strtok (NULL,"|"); p.plenary = atoi(s);  
			s = strtok (NULL,"|"); p.sessionId = atoi(s);
			s = strtok (NULL, "|"); p.chairName = s;    
			s = strtok (NULL,"|"); p.durationTime = atoi(s);  p.passageOrder = -1;   	      
			s = strtok (NULL, "|"); p.timeZone = atof(s); 

			
			
			vector<string> cd1;
			vector<string> cd2;
			vector<string> cd3;

			int i,j,m;

		

			//constraint 1
			s = strtok (NULL, "|");
			cd1 = split(s, ",");
			

			vector<string> bound; Constraint c;tm start;tm end;
			for(i=0; i< cd1.size(); i++)
			{
				bound = split(cd1.at(i), "-");

				if(bound.size()>1)
				{
					for(j=0; j<bound.size();j++)
					{
						

						if(j==0)
						{
							
							vector<string> timeStartHM;
							timeStartHM= split(bound.at(j), ":");
							
							start.tm_hour = atoi(timeStartHM.at(0).c_str());
							start.tm_min = atoi(timeStartHM.at(1).c_str());
							c.start =start;

						}
						else
						{
							vector<string> timeEndHM;
							timeEndHM = split(bound.at(j), ":");
							end.tm_hour = atoi(timeEndHM.at(0).c_str());
							end.tm_min = atoi(timeEndHM.at(1).c_str());
							c.end = end;
						}


					}

					p.constraintsDay1.push_back(c);
				}

				
			}
			
			s = strtok (NULL, "|");
			cd2 = split(s, ",");

			for(i=0; i< cd2.size(); i++)
			{
				
				bound = split(cd2.at(i), "-");

				if(bound.size()>1)
				{
					for(j=0; j<bound.size();j++)
					{
						if(j==0)
						{
							vector<string> timeStartHM;
							timeStartHM= split(bound.at(j), ":");
							start.tm_hour = atoi(timeStartHM.at(0).c_str());
							start.tm_min = atoi(timeStartHM.at(1).c_str());
							c.start =start;
						}
						else
						{
							vector<string> timeEndHM;
							timeEndHM = split(bound.at(j), ":");
							end.tm_hour = atoi(timeEndHM.at(0).c_str());
							end.tm_min = atoi(timeEndHM.at(1).c_str());
							c.end = end;
						}
					}

					p.constraintsDay2.push_back(c);
				}
				
				
			}
			
			s = strtok (NULL, "|");
			cd3 = split(s, ",");

			for(i=0; i< cd3.size(); i++)
			{
				
				bound = split(cd3.at(i), "-");
				if(bound.size()>1)
				{

					for(j=0; j<bound.size();j++)
					{
						if(j==0)
						{
							vector<string> timeStartHM;
							timeStartHM= split(bound.at(j), ":");
							start.tm_hour = atoi(timeStartHM.at(0).c_str());
							start.tm_min = atoi(timeStartHM.at(1).c_str());
							c.start =start;
						}
						else
						{
							vector<string> timeEndHM;
							timeEndHM = split(bound.at(j), ":");
							end.tm_hour = atoi(timeEndHM.at(0).c_str());
							end.tm_min = atoi(timeEndHM.at(1).c_str());
							c.end = end;
						}
					}
					p.constraintsDay3.push_back(c);
				}
				
			}

			papers.push_back(p);
		}

		//parcours de la liste des papier
		vector<int> existingTrackId;
		vector<int> existingSessionId;

		//parcours des papiers
		for(k=0;k<papers.size();k++)
		{
			//si le track courant n'a pas encore étét traité
			if(!contains(existingTrackId, papers.at(k).trackId))
			{
				Track t;
				t.trackId = papers.at(k).trackId;
				existingTrackId.push_back(t.trackId);
				tracks.push_back(t);

			}

			//si la session courante n'a pas encore été traitée
			if(!contains(existingSessionId, papers.at(k).sessionId))
			{
				Session s;
				s.sessionId = papers.at(k).sessionId;
				s.isPlenary = papers.at(k).plenary;

				existingSessionId.push_back(s.sessionId);
				s.papers.push_back(papers.at(k));				
				tracks.back().sessions.push_back(s);
			}
			else //sinon ajoute la papier à la dernière session stockée du dernier 	track stocké
			{
				if(tracks.back().sessions.size()>0)
					tracks.back().sessions.back().papers.push_back(papers.at(k));
				else
					cout << "ATTENTION: papier " << papers.at(k).paperId << " rejeté" << endl << endl;
			}
		}
		

		fPapers.close();
	}	
}

void displayDateTime(tm t, const char* format)
{
	char buffer[80];
	strftime(buffer, 80, format, &t);
	cout << buffer;
}

void displayTimeConstraints(vector<Constraint> constraints)
{
	int i;

	for (i = 0; i < constraints.size(); i++)
	{
		Constraint constraint = constraints.at(i);
		displayDateTime(constraint.start, "%R");
		cout << " - ";
		displayDateTime(constraint.end, "%R");
		if (i != constraints.size()-1)
		{
			cout << ", ";
		}
	}
	cout << endl;
}

void displayPaper(Paper p)
{
	cout << "\t\tPaper: " << p.paperId << " - Chair: " << p.chairName << endl;
	cout << "\t\tSession: " << p.sessionId << " - Passage Order: " << p.passageOrder << " - Duration: " << p.durationTime << "min" << endl;
	cout << "\t\tConstraints day 1: ";
	displayTimeConstraints(p.constraintsDay1);
	cout << "\t\tConstraints day 2: ";
	displayTimeConstraints(p.constraintsDay2);
	cout << "\t\tConstraints day 3: ";
	displayTimeConstraints(p.constraintsDay3);
}

void displaySession(Session s)
{
	int i;

	cout << "\t--- Session: " << s.sessionId << " - Plenary: " << (s.isPlenary ? "true" : "false") << " ---" << endl;
	cout << "\tStart date: ";
	displayDateTime(s.start, "%m/%d/%Y %H:%M");
	cout << " - End date: ";
	displayDateTime(s.end, "%m/%d/%Y %H:%M");
	cout << endl << endl;

	for (i = 0; i < s.papers.size(); i++)
	{
		displayPaper(s.papers.at(i));
		cout << endl;
	}
}

void displayTrack(Track t)
{
	int i;

	cout << "===== Track " << t.trackId << " =====" << endl;

	for (i = 0; i < t.sessions.size(); i++)
	{
		displaySession(t.sessions.at(i));
	}
}

void displayTracks(vector<Track> tracks)
{
	int i;
	for (i = 0; i < tracks.size(); i++)
	{
		displayTrack(tracks.at(i));
	}
}

void initDates()
{
	strptime(START, DATE_FORMAT, &startDate);
	startDate.tm_isdst = DST;

	strptime(END, DATE_FORMAT, &endDate);
	endDate.tm_isdst = DST;

	time_t temp = mktime(&startDate)-startDate.tm_min*60-startDate.tm_hour*60*60;
	time_t end = mktime(&endDate);
	while (difftime(end, temp) > 0)
	{
		tm dateToPush = *localtime(&temp);
		dateToPush.tm_isdst = DST;
		dates.push_back(dateToPush);
		temp += 24*60*60;
	}
}

int normaliseMinutes(int nbMinutes)
{
	int found = false;
	int i = 0;
	while (!found && i < 12)
	{
		if (nbMinutes < minutes[i])
		{
			nbMinutes = minutes[i-1];
			found = true;
		}
		else
		{
			i++;
		}
	}
	if (!found)
	{
		nbMinutes = 55;
	}
	return nbMinutes;
}

tm createRandomDate()
{
	int index = rand() % dates.size();
	tm date = dates.at(index);
	int minutes;
	if (index == 0 && index == dates.size()-1)
	{
		int startMinutes = startDate.tm_hour*60 + startDate.tm_min;
		int endMinutes = endDate.tm_hour*60 + endDate.tm_min;
		minutes = rand() % (endMinutes-startMinutes) + startMinutes;
	}
	else if (index == 0)
	{
		int startMinutes = startDate.tm_hour*60 + startDate.tm_min;
		minutes = rand() % (24*60-startMinutes) + startMinutes;	
	}
	else if (index == dates.size()-1)
	{
		int endMinutes = endDate.tm_hour*60 + endDate.tm_min;
		minutes = rand() % endMinutes;
	}
	else
	{
		minutes = rand() % (24*60);
	}
	int hours = (int)minutes/60;
	date.tm_hour = hours;
	date.tm_min = normaliseMinutes(minutes - hours*60);
	return date;
}

tm computeEndDateSession(Session s)
{
	time_t nbSeconds = mktime(&s.start);
	tm res;
	int i;
	for (i = 0; i < s.papers.size(); i++)
	{
		nbSeconds += s.papers.at(i).durationTime*60;
	}
	res = *localtime(&nbSeconds);
	res.tm_isdst = DST;
	return res;
}

int main()
{
	initDates();
	readPapersFile();

	int i, j, k;
	for (i = 0; i < tracks.size(); i++)
	{
		vector<Session> * sessions = &tracks.at(i).sessions;
		for (j = 0; j < sessions->size(); j++)
		{
			Session * s = &sessions->at(j);
			s->start = createRandomDate();
			s->end = computeEndDateSession(*s);
			int passageOrders[s->papers.size()];
			for (k = 0; k < s->papers.size(); k++)
			{
				passageOrders[k] = k;
			}
			for (k = 0; k < s->papers.size(); k++)
			{
				int index = rand() % (s->papers.size()-k) + k;
				int passageOrder = passageOrders[index];
				s->papers.at(k).passageOrder = passageOrder+1;
				passageOrders[index] = passageOrders[k];
				passageOrders[k] = passageOrder;
			}
		}
	}

	displayTracks(tracks);
	
	return 0;
}